package com.jjesus.callback;

import com.jjesus.callback.Caller;
import com.jjesus.callback.Handler;

public class App
{
    public static void main( String[] args )
    {
        System.out.println("Main()");
        Caller caller = new Caller();
        Handler handler = new Handler();
        caller.registerCallback(handler);
        caller.doCallback();
    }
}

