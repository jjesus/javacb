
## Creation
Go up one directory to create.

Created with maven-archetype-quickstart

    mvn archetype:generate  \
      -DgroupId=com.jjesus.callback \
      -DartifactId=javacb \
      -DarchetypeArtifactId=maven-archetype-quickstart \
      -DinteractiveMode=false

## Third party jar
    mvn install:install-file \
        -Dfile=/home/jjjesus/Apps/jnlua-0.9.5/target/jnlua-0.9.5.jar \
        -DgroupId=com.naef \
        -DartifactId=jnlua \
        -Dversion=0.9.5 \
        -Dpackaging=jar

## Build
    cd javacb
    mvn package

## Run
    cd javacb
    java -cp target/javacb-1.0-SNAPSHOT.jar:/home/jjjesus/.m2/repository/com/naef/jnlua/0.9.5/jnlua-0.9.5.jar \
        com.jjesus.callback.App

