package com.jjesus.callback;

import com.jjesus.callback.Callback;

import com.naef.jnlua.LuaState;

public class Handler implements Callback {
    public void callback()
    {
        System.out.println("Handler::callback()");
        // Create a Lua state
        LuaState luaState = new LuaState();
        try {
            // Define a function
            luaState.load("function add(a, b) return a + b end", "=simple");

            // Evaluate the chunk, thus defining the function
            luaState.call(0, 0); // No arguments, no returns

            // Prepare a function call
            luaState.getGlobal("add"); // Push the function on the stack
            luaState.pushInteger(1); // Push argument #1
            luaState.pushInteger(1); // Push argument #2

            // Call
            luaState.call(2, 1); // 2 arguments, 1 return

            // Get and print result
            int result = luaState.toInteger(1);
            luaState.pop(1); // Pop result
            System.out.println("According to Lua, 1 + 1 = " + result);
        } finally {
                luaState.close();
        }
    }
}

