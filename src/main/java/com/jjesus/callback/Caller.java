package com.jjesus.callback;

import com.jjesus.callback.Handler;

/**
 * Hello world!
 *
 */
public class Caller
{
    Handler handler;

    public void registerCallback(Handler handler) {
        System.out.println( "registerCallback" );
        this.handler = handler;
    }

    public void doCallback() {
        handler.callback();
    }
}

